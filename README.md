## ITRI DARS description

This is the URDF model of ITRI's dual arm robot system.
We break the whole robot into three parts(torso, arm and hand).

## Recommendation environment setting
Ubuntu:16.04 or 18.04 

ROS distribution： kinetic or melodic

## Installation

create a catkin workspace and clone our package into src directory
<pre><code>$ git clone https://gitlab.com/itrirobotics/dars/dual_arm_model.git
</code></pre>

Run catkin_make within the workspace.
<pre><code>$ catkin_make
$ source devel/setup.bash
</code></pre>

## Run the program

View the DARS model in Rviz
<pre><code> roslaunch dars_description display_dars.launch
</code></pre>

View torso only
<pre><code> roslaunch dars_description display_torso.launch
</code></pre>

View sj705 only
<pre><code> roslaunch dars_description display_sj705.launch
</code></pre>

View right_ochu only
<pre><code> roslaunch dars_description display_right_ochu.launch
</code></pre>

## Description
the files in urdf directory

- sj705_macro.xacro
   the URDF model of arm
   
- torso_macro.xacro
   the URDF model of torso
   
- right_ochu_macro.xacro
   the URDF model of right ochu

- dars.xacro
   include the above three files
  
- torso.xacro
   include torso_macro.xacro only
    
- sj705.xacro
   include sj705_macro.xacro only
    
- right_ochu.xacro
   include right_ochu_macro.xacro only

## Authors

-  Chien-Lin Tseng  
    email: daniel0978535679@gmail.com

-  Chen-Ting Lin  
    email: lct0608.eed05@g2.nctu.edu.tw 

  
- ITRI Robotics
